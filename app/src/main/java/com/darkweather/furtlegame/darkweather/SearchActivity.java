package com.darkweather.furtlegame.darkweather;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class SearchActivity extends AppCompatActivity {
    EditText mytext;
    Typeface typeface;
    Button nextButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mytext = findViewById(R.id.editText);
        typeface = Typeface.createFromAsset(this.getAssets(), "fonts/YBTallPretty.ttf");
        mytext.setTypeface(typeface);

        nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SearchActivity.this,WeatherActivity.class);
                startActivity(i);
            }
        });
    }
}
