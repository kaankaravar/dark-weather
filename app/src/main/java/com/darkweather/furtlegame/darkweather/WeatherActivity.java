package com.darkweather.furtlegame.darkweather;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class WeatherActivity extends AppCompatActivity {
    Typeface typeface;

    Button _backButton;
    ImageView _mainWeatherImage;
    TextView _mainWeatherText;
    TextView _mainCityText;
    TextView _mainWeatherStatus;

    TextView day1Title;
    ImageView day1Image;
    TextView day1Weather;

    TextView day2Title;
    ImageView day2Image;
    TextView day2Weather;

    TextView day3Title;
    ImageView day3Image;
    TextView day3Weather;

    TextView day4Title;
    ImageView day4Image;
    TextView day4Weather;

    TextView day5Title;
    ImageView day5Image;
    TextView day5Weather;

    TextView day6Title;
    ImageView day6Image;
    TextView day6Weather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        typeface = Typeface.createFromAsset(this.getAssets(), "fonts/YBTallPretty.ttf");

        _backButton = findViewById(R.id.backButton);
        _mainWeatherImage = findViewById(R.id.imageMainWeather);
        _mainWeatherText = findViewById(R.id.mainWeatherText);
        _mainCityText = findViewById(R.id.mainCityText);
        _mainWeatherStatus = findViewById(R.id.mainWeatherStatus);

        day1Title = findViewById(R.id.day1TitleText);
        day1Image = findViewById(R.id.imageDay1);
        day1Weather = findViewById(R.id.weatherDay1Text);

        day2Title = findViewById(R.id.day2TitleText);
        day2Image = findViewById(R.id.imageDay2);
        day2Weather = findViewById(R.id.weatherDay2Text);

        day3Title = findViewById(R.id.day3TitleText);
        day3Image = findViewById(R.id.imageDay3);
        day3Weather = findViewById(R.id.weatherDay3Text);

        day4Title = findViewById(R.id.day4TitleText);
        day4Image = findViewById(R.id.imageDay4);
        day4Weather = findViewById(R.id.weatherDay4Text);

        day5Title = findViewById(R.id.day5TitleText);
        day5Image = findViewById(R.id.imageDay5);
        day5Weather = findViewById(R.id.weatherDay5Text);

        day6Title = findViewById(R.id.day6TitleText);
        day6Image = findViewById(R.id.imageDay6);
        day6Weather = findViewById(R.id.weatherDay6Text);



        _mainWeatherImage.setImageResource(R.drawable.logo); // Image değiştirme


        _mainWeatherText.setTypeface(typeface);
        _mainWeatherText.setTypeface(typeface);
        _mainCityText.setTypeface(typeface);
        day1Title.setTypeface(typeface);
        day1Weather.setTypeface(typeface);
        day2Title.setTypeface(typeface);
        day2Weather.setTypeface(typeface);
        day3Title.setTypeface(typeface);
        day3Weather.setTypeface(typeface);
        day4Title.setTypeface(typeface);
        day4Weather.setTypeface(typeface);
        day5Title.setTypeface(typeface);
        day5Weather.setTypeface(typeface);
        day6Title.setTypeface(typeface);
        day6Weather.setTypeface(typeface);


    }
}
