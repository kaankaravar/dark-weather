package com.darkweather.furtlegame.darkweather;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button chooseButton;
    Typeface myfont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chooseButton = findViewById(R.id.startButton);
        myfont = Typeface.createFromAsset(this.getAssets(), "fonts/YBTallPretty.ttf");
        chooseButton.setTypeface(myfont);

        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent i = new Intent(MainActivity.this,SearchActivity.class);
              startActivity(i);
            }
        });
    }
}
